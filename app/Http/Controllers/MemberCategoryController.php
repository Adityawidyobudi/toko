<?php

namespace App\Http\Controllers;

use App\MemberCategory;
use Illuminate\Http\Request;

class MemberCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $errors = MemberCategory::all();
        return view('member_category.index', compact('errors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('member_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $errors = new MemberCategory;
        $errors->name = $request->name;
        $errors->discount = $request->discount;
        $errors->save();
        return redirect()->route('member_category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MemberCategory  $memberCategory
     * @return \Illuminate\Http\Response
     */
    public function show(MemberCategory $memberCategory)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MemberCategory  $memberCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $errors = MemberCategory::findOrFail($id);
        return view('member_category.edit', compact('errors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MemberCategory  $memberCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $errors = MemberCategory::findOrFail($id);
        $errors->name = $request->name;
        $errors->discount = $request->discount;
        $errors->update();
        return redirect()->route('member_category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MemberCategory  $memberCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $errors = MemberCategory::findOrFail($id);
        $errors->delete();
        return redirect()->route('member_category.index');
    }
}
