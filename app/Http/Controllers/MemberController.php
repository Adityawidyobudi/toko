<?php

namespace App\Http\Controllers;

use App\Member;
use App\MemberCategory;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $errors = Member::with('categoryRef')->get();
        return view('member.index', compact('errors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $errors = MemberCategory::all();
        return view('member.create', compact('errors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $member = new Member;
       $member->member_category_id = $request->errors;
       $member->full_name = $request->full_name;
       $member->dob = $request->dob;
       $member->address = $request->address;
       $member->gender = $request->gender;
       $barcode = str_replace("-","",$request->date_of_birth).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
       $member->barcode = $barcode;
       $member->save();
       return redirect()->route('member.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Member $member)
    {
        $member = member::with('pembeli.categoryRef')->findorFail($member->id);
        return view('member.history', compact('member')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function edit(Member $member)
    {
        $errors = MemberCategory::all();
        return view('member.edit', compact('errors', 'member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member)
    {
       
       $member->member_category_id = $request->errors;
       $member->full_name = $request->full_name;
       $member->dob = $request->dob;
       $member->address = $request->address;
       $member->gender = $request->gender;
       $barcode = str_replace("-","",$request->date_of_birth).substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,3);
       $member->barcode = $barcode;
       $member->update();
       return redirect()->route('member.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(Member $member)
    {
        $member->delete();
        return redirect()->route('member.index');
    }
}
