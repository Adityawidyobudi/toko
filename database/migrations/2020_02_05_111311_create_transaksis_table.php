<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trx_number');
            $table->unsignedBigInteger('member_id');
            $table->unsignedBigInteger('product_id');
            $table->integer('quantity');
            $table->string('discount');
            $table->integer('total');
            $table->timestamps();

            $table->foreign('member_id')->refrences('id')->on('member');
            $table->foreign('product_id')->refrences('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
