@extends('layouts.template')

@section('content')


     
  <!-- Begin Page Content -->
  <div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-1 text-gray">Barang</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">
      <li>
        <a href="{{ route('product.create') }}">
          <button type="button" class="btn btn-primary btn-sm">Tambah</button>
        </a>
      </li>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr style="text-align: center;">
            <th>No</th>
            <th>Nama</th>
            <th>Gambar</th>
            <th>Produk_Kategori</th>
            <th>Deskripsi</th>
            <th>Harga</th>
            <th>Pilihan</th>
          </tr>
        </thead>
        
        <tbody>
              @foreach ($category as $i => $item)
              <tr style="text-align: center;">
                  <td>{{ $i+1 }}</td>
                  <td>{{ $item->name }}</td>
                  <td> <img src="{{ URL::to('/') }}/uploads/products/{{$item->image}}"
                  class="img-thumbnail" width="100px" height="100px" /></td>
                  <td>{{ $item->categoryRef->name }}</td>
                  <td>{{ $item->desc }}</td>
                  <td>{{ $item->price }}</td>
                  <td>
                    <form action="{{ route('product.destroy',$item->id) }}" method="post">
                    <a href="{{ route('product.edit',$item->id) }}" class="btn btn-success btn-circle"><i class="fas fa-edit"></i>
                    </a>
                    @csrf 
                    @method('delete')
                      <button type="submit" class="btn btn-danger btn-circle" 
                      onclick="return confirm('Yakin Nih Dihapus ?')"><i class="fas fa-trash"></i></button>
                      <a href="{{ route('product.show',$item->id) }}" class="btn btn-info btn-circle">
                    <i class="fas fa-list"></i>
                       </a>
                    </form>

                    </form>
                </td>
              </tr>
              @endforeach
              </tbody>  
        
      </table>
    </div>
  </div>
</div>
@endsection