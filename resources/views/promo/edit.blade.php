@extends('layouts.template')

@section('content')

<div class="container">
<div class="x_panel">
    <div class="x_title">
      <div class="card shadow mb-4">
  <div class="card-header py-2">
     <h1 class="h3 mb-1 text-gray">Ubah Data Promo</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">

    <div class="card body">                           
    <form action="{{ route('promo.update', $promo->id) }}" method="post">
    @csrf
    @method('put') 
    <form role="form">
        <div class="col-md-6">
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Masukkan Nama Barang" value="{{ $promo->name }}">
            </div>
        <div class="form-group">
                <label for="category">Nama</label>
                <select class="form-control" id="category" name="category">
                             <option>chose...</option>
                                @foreach ($category as $item)
                                <option value="{{ $item->id }}"> {{ $item->name }}</option>
                                @endforeach
                        </select>
            </div>

        <div class="form-group">
                <label for="discount">Diskon</label>
                <input type="text" name="discount" class="form-control" id="discount" placeholder="Masukkan Deskripsi Barang" value="{{ $promo->discount }}">
            </div>
            <button type="submit" class="btn btn-danger">Simpan</button>
            <a href="/promo" class="btn btn-warning">Kembali</a>
    </form>
    </div> 
    </div> 
    </div>
    </div>                                 
@endsection