@extends('layouts.template')

@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">


<!-- Page Heading -->
<div class="card shadow mb-4">
  <div class="card-header py-2">
     <h1 class="h3 mb-1 text-gray">Member Category</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">
        <li> 
          <a href="{{ route ('member_category.create') }}">
                      <button type="button" class="btn btn-primary btn-sm">Tambah</button>
          </a>
        </li>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
        <tr style="text-align: center;">
            <th>No</th>
            <th>Nama</th>
            <th>Diskon</th>
            <th>Pilihan</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($errors as $i => $item)
            <tr style="text-align: center;">
                <td>{{ $i+1 }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->discount }}%</td>

                <td>
                <form action="{{ route('member_category.destroy',$item->id) }}" method="post">
                    <a href="{{ route('member_category.edit',$item->id) }}" class="btn btn-success btn-circle"><i class="fas fa-edit"></i>
                    </a>
                    @csrf 
                    @method('delete')
                      <button type="submit" class="btn btn-danger btn-circle" 
                      onclick="return confirm('Yakin Nih Dihapus ?')"><i class="fas fa-trash"></i></button>
                    </form>
          </tr>
          @endforeach
        </tbody>
        
      </table>
    </div>
  </div>
</div>
@endsection