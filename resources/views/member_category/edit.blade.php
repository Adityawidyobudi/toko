@extends('layouts.template')

@section('content')

<div class="container">

    <div class="card body">                           
    <h1>Ubah Data Member Category</h1>
    <form action="{{ route('member_category.update', $errors->id) }}" method="post">
    @csrf
    @method('put') 
    <form role="form">
         <div class="col-md-6">
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Masukkan Nama Barang" value="{{ $errors->name }}">
            </div>
        <div class="form-group">
                <label for="discount">Diskon</label>
                <input type="text" name="discount" class="form-control" id="name" placeholder="Masukkan Nama Barang" value="{{ $errors->discount }}">
            </div>
        
            <button type="submit" class="btn btn-danger">Simpan</button>
            <a href="/member_category" class="btn btn-warning">Kembali</a>
    </form>
    </div> 
    </div>                                  
@endsection