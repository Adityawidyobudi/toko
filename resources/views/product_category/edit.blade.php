@extends('layouts.template')

@section('content')

<div class="container">

    <div class="card body">                           
    <h1>Ubah Data Barang</h1>
    <form action="{{ route('product_category.update', $category->id) }}" method="post">
    @csrf
    @method('put') 
    <form role="form">
         <div class="col-md-6">
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Masukkan Nama Barang" value="{{ $category->name }}">
            </div>
            <br />
        <div class="form-group">
                <label for="desc">Deskripsi</label>
                <input type="text" name="desc" class="form-control" id="desc" placeholder= "Masukkan Deskripsi Barang" value="{{ $category->desc }}">
            </div>
            <button type="submit" class="btn btn-danger">Simpan</button>
            <a href="/product_category" class="btn btn-warning">Kembali</a>
    </form>
    </div> 
    </div>                                  
@endsection