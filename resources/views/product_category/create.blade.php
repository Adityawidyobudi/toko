@extends('layouts.template')

@section('content')

<div class="container">
<div class="x_panel">
    <div class="x_title">
      <div class="card shadow mb-4">
  <div class="card-header py-2">
     <h1 class="h3 mb-1 text-gray">Tambah Data Barang</h1>

<!-- DataTales Example -->
<div class="card shadow mb-3">
  <div class="card-header py-2">

    <div class="card body">                           
    <form action="{{ route ('product_category.store') }}" method="post">
    @csrf 
    <form role="form">
         <div class="col-md-6">
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Masukkan Nama Barang">
            </div>
            <br />
        <div class="form-group">
                <label for="desc">Deskripsi</label>
                <input type="text" name="desc" class="form-control" id="desc" placeholder="Masukkan Deskripsi Barang">
            </div>
            <button type="submit" class="btn btn-danger">Simpan</button>
            <a href="/product_category" class="btn btn-warning">Kembali</a>
    </form>
    </div> 
    </div> 
    </div>
    </div>                                 
@endsection